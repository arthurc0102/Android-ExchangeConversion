package me.arthurc.exchangeconversion;

import android.content.res.Resources.NotFoundException;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.HashMap;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private View currentView;
    private Spinner spExchange1;
    private Spinner spExchange2;
    private EditText etMoney1;
    private EditText etMoney2;

    private HashMap exChangeMap = new HashMap<String, Double>() {{
        put("新臺幣", 29.527);
        put("人民幣", 6.21621);
        put("美金", 1.0);
        put("日幣", 107.13716);
        put("英鎊", 0.69443);
        put("歐元", 0.80346);
    }};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Close button setup
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);

        // View setup
        currentView = findViewById(android.R.id.content);
        spExchange1 = findViewById(R.id.sp_exchange_1);
        spExchange2 = findViewById(R.id.sp_exchange_2);
        etMoney1 = findViewById(R.id.et_money_1);
        etMoney2 = findViewById(R.id.et_money_2);

        // Listener setup
        findViewById(R.id.im_btn_swap).setOnClickListener(this);
        findViewById(R.id.btn_convert).setOnClickListener(this);

        // Spinner initialize
        ArrayAdapter<Object> spinnerOptions = new ArrayAdapter<>(
                this,
                android.R.layout.simple_spinner_dropdown_item,
                exChangeMap.keySet().toArray()
        );
        spExchange1.setAdapter(spinnerOptions);
        spExchange1.setSelection(0);
        spExchange2.setAdapter(spinnerOptions);
        spExchange2.setSelection(1);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return false;
    }

    private Snackbar s(Object textOrId) {
        String text;

        if (textOrId instanceof Integer) {
            try {
                text = getResources().getString((int) textOrId);
            } catch (NotFoundException e) {
                text = textOrId.toString();
            }
        } else {
            text = textOrId.toString();
        }

        return Snackbar.make(currentView, text, Snackbar.LENGTH_LONG);
    }

    private double dataValidation() {
        String money1 = etMoney1.getText().toString();

        if (money1.isEmpty()) {
            s("上方的輸入框不得為空白").show();
            return Double.NaN;
        }

        double result;

        try {
            result = Double.parseDouble(money1);
        } catch (Exception e) {
            s("輸入框只能輸入數字").show();
            return Double.NaN;
        }

        if (result < 0.0) {
            s("不得輸入小於 0 的數字").show();
            return Double.NaN;
        }

        return result;
    }

    private double convert(double money) {
        String from = spExchange1.getSelectedItem().toString();
        String to = spExchange2.getSelectedItem().toString();

        return money / (double) exChangeMap.get(from) * (double) exChangeMap.get(to);
    }

    private void onBtnSwapClick() {
        int tmpSelected = spExchange1.getSelectedItemPosition();
        spExchange1.setSelection(spExchange2.getSelectedItemPosition());
        spExchange2.setSelection(tmpSelected);

        etMoney1.setText("");
        etMoney2.setText("");
    }

    private void onBtnConvertClick() {
        Double money = dataValidation();
        if (Double.isNaN(money)) {
            return;
        }

        double result = convert(money);
        etMoney2.setText(String.valueOf(result));

        s("換算成功！").show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.im_btn_swap:
                onBtnSwapClick();
                break;

            case R.id.btn_convert:
                onBtnConvertClick();
                break;
        }
    }
}
